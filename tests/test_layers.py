#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

""" Test layers module """

# Imports
import pytest
import theano
import numpy as np
import theano.tensor as T
from unittest.mock import MagicMock
from theano.tensor.shared_randomstreams import RandomStreams
from aimlessarm.layers import BaseLayer, HiddenLayer,\
    ConvLayer, PoolLayer, DropoutLayer, InputLayer,\
    OutputLayer, get_name

# Utilities ########################################
# Mock out random state


class MockRandomState(object):
    """Mock out random state generator"""

    def __init__(self, seed, *args, **kwargs):
        self.seed = seed

    def uniform(self, low, high, size):
        """Mock out the uniform number generator"""

        return np.ones(size) * self.seed

    def binomial(self, *args, **kwargs):
        pass

# Mock out activation to check for correct calculation


class activation(object):

    def __init__(self):
        self.name = 'activation'

    def __call__(self, a):
        return 2 * a


activation = activation()

# Utilities tests


def test_get_name():
    """ Test the get name function """

    assert(get_name(T.tanh) == 'tanh')
    assert(get_name(T.nnet.sigmoid) == 'sigmoid')
    assert(get_name(activation) == 'activation')
    assert(get_name(BaseLayer) == 'BaseLayer')
    assert(get_name(None) == 'None')

# BaseLayer Tests ########################################


def test_baselayer_init():
    """Test initialisation of base layer"""

    rng = np.random.RandomState()
    x = T.scalar()
    input_shape = [11]
    output_shape = [500]
    activation = T.nnet.relu
    base_layer = BaseLayer(
        tensor_in=x,
        input_shape=input_shape,
        output_shape=output_shape,
        rng=rng,
        activation=activation)

    assert(base_layer.tensor_in == x)
    assert(base_layer.input_shape == input_shape)
    assert(base_layer.output_shape == output_shape)
    assert(base_layer.rng == rng)
    assert(base_layer.activation == activation)
    assert(base_layer.W.eval().shape == (11, 500))
    assert(base_layer.b.eval().shape == (500,))


def test_baselayer_n_in():
    """ Test number of input weights for BaseLayer """

    rng = np.random.RandomState()
    x = T.scalar()
    input_shape = [None, 3, 62, 62]
    output_shape = [500]
    activation = T.nnet.relu
    base_layer = BaseLayer(
        tensor_in=x,
        input_shape=input_shape,
        output_shape=output_shape,
        rng=rng,
        activation=activation)

    assert(base_layer.W.eval().shape == (3 * 62 * 62, 500))


def test_baselayer_missing_rng():
    """ Test error raised when missing rng """

    x = T.scalar()
    input_shape = [None, 3, 62, 62]
    output_shape = [500]
    activation = T.nnet.relu

    with pytest.raises(ValueError) as e:
        base_layer = BaseLayer(
            tensor_in=x,
            input_shape=input_shape,
            output_shape=output_shape,
            activation=activation)
        assert('Missing rng' in str(e))


def test_baselayer_str():
    """Test the BaseLayer string"""
    rng = np.random.RandomState()
    x = T.scalar()
    input_shape = [11]
    output_shape = [500]
    activation = T.nnet.relu
    base_layer = BaseLayer(
        tensor_in=x,
        input_shape=input_shape,
        output_shape=output_shape,
        rng=rng,
        activation=activation)

    expected_str = "{:<10}: [11]x[500] {:<10}relu".format(
        'BaseLayer', 'Activation: ')

    assert(str(base_layer) == expected_str)


def test_baselayer_missing_params():
    """ Test errors raised for BaseLayer missing params """

    with pytest.raises(ValueError) as e:
        BaseLayer()
        assert('Missing tensor_in' in str(e))

    with pytest.raises(ValueError) as e:
        BaseLayer(tensor_in=None)
        assert('Missing input_shape' in str(e))


# InputLayer Tests ########################################

def test_inputlayer_init():
    """ Test the init of the InputLayer """

    x = T.matrix()
    input_shape = 11

    obj = InputLayer(
        tensor_in=x,
        output_shape=4,
        input_shape=input_shape)

    assert(obj.output == x)
    assert(str(obj) == "InputLayer: [11] Activation: None")


def test_inputlayer_compute_bounds():
    """ Test inputlayer compute bounds """

    x = T.matrix()
    input_shape = 11

    obj = InputLayer(
        tensor_in=x,
        output_shape=4,
        input_shape=input_shape)
    result = obj._compute_bounds()

    assert(result == (None, None, None))


# HiddenLayer Tests ########################################


def test_hidden_layer_init():
    """ Test Hidden layer flattening """

    x = T.tensor4()
    input_shape = [1, 1, 2, 3]
    output_shape = 6

    obj = HiddenLayer(
        tensor_in=x,
        input_shape=input_shape,
        output_shape=output_shape,
        rng=np.random.RandomState(0),
    )

    output = theano.function(inputs=[x],
                             outputs=obj.tensor_in)

    result = output(np.ones(input_shape))

    assert(len(result.shape) == 2)


def test_hiddenlayer_str():
    """Test the HiddenLayer string"""
    rng = np.random.RandomState()
    x = T.scalar()
    input_shape = 11
    output_shape = 500
    activation = T.nnet.relu
    base_layer = HiddenLayer(
        tensor_in=x,
        input_shape=input_shape,
        output_shape=output_shape,
        rng=rng,
        activation=activation)

    expected_str = "{:<10}: [11]x[500] {:<10}relu".format(
        'HiddenLayer', 'Activation: ')

    assert(str(base_layer) == expected_str)


def simple_layer_compute(input_shape, output_shape, rng, layer=BaseLayer):
    """Test base layer computes correctly"""

    x = T.matrix()

    compute_layer = layer(
        tensor_in=x,
        input_shape=input_shape,
        output_shape=output_shape,
        rng=rng,
        activation=activation)

    W = compute_layer.W.eval()
    b = compute_layer.b.eval()

    # Test the values y = Wx + b
    x_in = np.array([[1], [3]], dtype=theano.config.floatX)
    y_expected = np.array([[4, 4], [8, 8]])
    y = compute_layer.output.eval({x: x_in})

    # Tests
    assert(W.shape == (1, 2))
    assert(b.shape == (2,))
    np.testing.assert_almost_equal(W, np.ones((1, 2)))
    np.testing.assert_almost_equal(b, np.ones((2)))
    np.testing.assert_almost_equal(y_expected, y)


def test_baselayer_compute(mock):
    """Test the base layer computes correctly"""

    rng = MockRandomState(1)  # Generate 1s
    mock.spy(rng, 'uniform')

    simple_layer_compute([1], [2], rng, layer=BaseLayer)

    assert(rng.uniform.call_count == 2)


def test_hiddenlayer_compute(mock):
    """Test the hidden layer computes correctly"""

    rng = MockRandomState(1)  # Generate 1s
    mock.spy(rng, 'uniform')

    simple_layer_compute(1, 2, rng, layer=HiddenLayer)

    assert(rng.uniform.call_count == 2)


# OutputLayer Tests ########################################

def test_output_layer():
    """ Test output layer name """

    x = T.matrix()
    obj = OutputLayer(
        tensor_in=x,
        input_shape=10,
        output_shape=10,
        rng=np.random.RandomState(0),
    )

    assert(obj.__qualname__ == 'OutputLayer')


# Convolution Layer Tests ########################################

def test_cov_layer_init(mock):
    """ Test convolutional init - defaults """

    rng = MockRandomState(1)
    mock.spy(rng, 'uniform')
    x = T.tensor4()

    cov_layer = ConvLayer(
        tensor_in=x,
        input_shape=[None, 1, 5, 5],  # No batches * 5x5 RBG Image
        output_shape=[None, 6, 4, 4],
        filter_shape=[6, 1, 2, 2],  # 6 x 1 x 1 filter
        rng=rng
    )

    assert(cov_layer.__qualname__ == 'ConvLayer')
    assert(cov_layer.border_mode == 'valid')
    assert(rng.uniform.call_count == 2)

    # Check shapes
    assert(cov_layer.filter_shape == [6, 1, 2, 2])
    assert(cov_layer.W.eval().shape == (6, 1, 2, 2))
    assert(cov_layer.b.eval().shape == (6,))
    output = cov_layer.output.eval({x: np.ones((1, 1, 5, 5))})
    assert(output.shape == (1, 6, 4, 4))


def test_conv_layer_shape_error():
    """ Test error is raised if filter_shape is not provided """

    with pytest.raises(ValueError) as m:
        ConvLayer()
        assert('ConvLayer missing filter_shape')


def test_cov_layer_compute():
    """ Test convolutional computations """

    rng = MockRandomState(0.5)
    x = T.tensor4()

    cov_layer = ConvLayer(
        tensor_in=x,
        input_shape=[None, 1, 5, 5],  # No batches * 3 x 3 Grayscale Image
        output_shape=[None, 3, 4, 4],  # No batches 3 deep 4x4
        filter_shape=[3, 1, 2, 2],  # 3 x 2 x 2 filter
        rng=rng,
        activation=None
    )

    # Check weights
    expected_W = np.ones((3, 1, 2, 2)) * 0.5
    expected_b = np.ones((3,)) * 0.5

    assert(np.all(cov_layer.W.eval() == expected_W))
    assert(np.all(cov_layer.b.eval() == expected_b))

    # Inputs and outputs
    input_arr = np.array([i for i in range(25)])
    input_arr = input_arr.reshape((1, 1, 5, 5))

    expected_output = np.array([
        [6.5,  8.5,  10.5, 12.5],
        [16.5, 18.5, 20.5, 22.5],
        [26.5, 28.5, 30.5, 32.5],
        [36.5, 38.5, 40.5, 42.5],
    ] * 3)
    expected_output = expected_output.reshape((1, 3, 4, 4))

    cov_output = cov_layer.output.eval({x: input_arr})
    assert(np.all(cov_output == expected_output))


def test_cov_layer_str():
    """ Test the output of the Convolutional layer string """
    rng = MockRandomState(0.5)
    x = T.tensor4()

    cov_layer = ConvLayer(
        tensor_in=x,
        input_shape=[None, 1, 5, 5],  # No batches * 3 x 3 Grayscale Image
        output_shape=[None, 3, 4, 4],  # No batches 3 deep 4x4
        filter_shape=[3, 1, 2, 2],  # 3 x 2 x 2 filter
        rng=rng,
        activation=T.nnet.relu
    )

    expected_str = "ConvLayer : [None, 1, 5, 5]o[3, 1, 2, 2]x[None, 3, 4, 4] "\
        "Activation: relu"

    assert(expected_str == str(cov_layer))


# Pooling Layer Tests ########################################

def test_pool_layer_init():
    """ Test pooling layer initialisation """

    x = T.matrix()
    p_layer = PoolLayer(tensor_in=x, input_shape=2, output_shape=1)

    # Test defaults
    assert(p_layer.__qualname__ == "PoolLayer")
    assert(p_layer.ws == (2, 2))
    assert(p_layer.ignore_border == False)
    assert(p_layer.stride == None)
    assert(p_layer.mode == 'max')
    assert(p_layer.pad == (0, 0))
    assert(p_layer.activation == None)

    # Check weights have been removed
    assert(hasattr(p_layer, 'W') == False)
    assert(hasattr(p_layer, 'b') == False)
    assert(hasattr(p_layer, 'params') == False)


def test_pool_layer_output():
    """ Testing pooling layer output """

    input_val = np.array([[
        [
            [6.5,  8.5],
            [12.5, 14.5],
        ],
        [
            [6.5,  8.5],
            [12.5, 14.5],
        ],
        [
            [6.5,  8.5],
            [12.5, 14.5],
        ],
    ]])
    input_shape = input_val.shape

    expected_output = np.array([
        [
            [14.5],
        ],
        [
            [14.5],
        ],
        [
            [14.5],
        ],
    ])
    output_shape = expected_output.shape

    x = T.tensor4()
    p_layer = PoolLayer(
        tensor_in=x,
        input_shape=input_shape,
        output_shape=output_shape)

    output = p_layer.output.eval({x: input_val})
    assert(np.all(output == expected_output))
    assert(p_layer.input_shape == input_shape)
    assert(p_layer.output_shape == output_shape)


def test_pool_layer_output_activation():
    """ Testing pooling layer output - activation"""

    class HalfActivation(object):

        def __call__(self, input_val):
            return 0.5 * input_val

    input_val = np.array([[
        [
            [6.5,  8.5],
            [12.5, 14.5],
        ],
        [
            [6.5,  8.5],
            [12.5, 14.5],
        ],
        [
            [6.5,  8.5],
            [12.5, 14.5],
        ],
    ]])
    input_shape = input_val.shape

    expected_output = np.array([
        [
            [7.25],
        ],
        [
            [7.25],
        ],
        [
            [7.25],
        ],
    ])
    output_shape = expected_output.shape

    x = T.tensor4()
    p_layer = PoolLayer(
        tensor_in=x,
        input_shape=input_shape,
        output_shape=output_shape,
        activation=HalfActivation(),
    )

    output = p_layer.output.eval({x: input_val})
    assert(np.all(output == expected_output))
    assert(p_layer.input_shape == input_shape)
    assert(p_layer.output_shape == output_shape)


# DropoutLayer Tests ########################################
def test_dropoutlayer_eval():
    """ Test evalulation of dropout layer """

    x = T.matrix()
    enable = T.bscalar()
    rng = np.random.RandomState(0)

    arr_in = np.ones((800, 800))
    for rate in [0.2, 0.5, 0.8]:
        drop = DropoutLayer(
            tensor_in=x, input_shape=arr_in.shape, rng=rng, p=rate, enable=enable)

        fn = theano.function([x, enable], drop.output)

        dropout_result = fn(arr_in, True)
        assert(np.any(dropout_result == 0))
        result = np.sum(dropout_result) / 10000

        np.testing.assert_almost_equal(result,
                                       (1 - rate) * np.prod(arr_in.shape) / 10000, decimal=1)


def test_dropout_eval_not_enabled():
    """ Test the output of DropoutLayer when not enabled """
    x = T.matrix()
    enable = T.bscalar()
    rng = np.random.RandomState(0)

    arr_in = np.ones((800, 800))
    for rate in [0.2, 0.5, 0.8]:
        drop = DropoutLayer(
            tensor_in=x, input_shape=arr_in.shape, rng=rng, p=rate, enable=enable)

        fn = theano.function([x, enable], drop.output)

        dropout_result = fn(arr_in, True)
        assert(np.any(dropout_result == 0))
        result = np.sum(dropout_result) / 10000

        np.testing.assert_almost_equal(result,
                                       (1 - rate) * np.prod(arr_in.shape) / 10000, decimal=1)


def test_dropout_str():
    """ Test evalulation of dropout layer """

    x = T.matrix()
    enable = T.bscalar()
    rng = np.random.RandomState(0)

    drop = DropoutLayer(tensor_in=x, input_shape=[
                        1], rng=rng, p=0.5, enable=enable)

    assert(str(drop) == "Dropout Layer: p = 0.50")


def test_drop_missing_enable():
    """ Test dropout layer missing enable tensor """

    x = T.matrix()
    rng = np.random.RandomState(0)

    with pytest.raises(AttributeError) as e:
        drop = DropoutLayer(tensor_in=x, input_shape=[1], rng=rng, p=0.5)
        assert('missing enable' in str(e))


def test_default_dropout_rate():
    """ Test the default dropout rate """

    x = T.matrix()
    enable = T.bscalar()
    rng = np.random.RandomState(0)
    drop = DropoutLayer(tensor_in=x, input_shape=[1], rng=rng, enable=enable)

    assert(drop.p == 0.5)
