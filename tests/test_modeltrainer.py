#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""
Test model trainer

:author: Ben Johnston

"""

# Imports
import pytest
import theano
import numpy as np
import theano.tensor as T
from unittest.mock import patch, mock_open, MagicMock,\
    ANY
from aimlessarm.modelBuilder import ModelBuilder
from aimlessarm.modelTrainer import ModelTrainer,\
    FixedLearnRate, FixedValue, FixedMomentum,\
    ScheduledValue, ScheduledLearnRate,\
    ScheduledMomentum


def test_fixed_value():
    """ Test the fixed value class """

    obj = FixedValue(0.1)
    assert(obj() == 0.1)


def test_fixed_momentum():
    """ Test the fixed momentum class """

    obj = FixedMomentum(0.9)
    assert(obj() == 0.9)
    assert(str(obj) == "Fixed Momentum: 0.900")


def test_fixed_momentum():
    """ Test the fixed learning rate """

    obj = FixedLearnRate(0.01)
    assert(obj() == 0.01)
    assert(str(obj) == "Fixed Learning Rate: 1.000000E-02")


def test_scheduled_value():
    """ Test scheduled value class """

    obj = ScheduledValue([0.1, 0.2, 0.3], [10, 20, 30])

    for i in range(40):
        val = obj()

        if i < 10:
            assert(val == 0.1)
        elif (i >= 10) and (i < 20):
            assert(val == 0.2)
        elif (i >= 30):
            assert(val == 0.3)


def test_scheduled_learnrate():
    """ Test scheduled learning rate """

    obj = ScheduledLearnRate([0.1, 0.2, 0.3], [10, 20, 30])

    assert(
        str(obj) == "Scheduled Learning Rate: [0.1, 0.2, 0.3] @ [10, 20, 30]")


def test_scheduled_momentum():
    """ Test scheduled learning rate """

    obj = ScheduledMomentum([0.1, 0.2, 0.3], [10, 20, 30])

    assert(str(obj) == "Scheduled Momentum: [0.1, 0.2, 0.3] @ [10, 20, 30]")


@pytest.fixture
def model():
    """ Generate the OR gate training model """
    xtrain = theano.shared(np.array([
        [1, 0],
        [1, 1],
        [0, 0],
        [1, 0],
        [0, 0],
    ], dtype=theano.config.floatX), borrow=True)

    ytrain = theano.shared(np.array([
        [1],
        [1],
        [0],
        [1],
        [0],
    ], dtype=theano.config.floatX), borrow=True)

    # Train to zero error - do not use validation set
    train_data = (xtrain, xtrain, ytrain, ytrain)

    # Construct simple model
    x = T.matrix()
    y = T.matrix()

    units = [2, 2, 1]
    rng = np.random.RandomState(0)

    model = ModelBuilder(x, y,
                         units=units,
                         layers=['InputLayer', 'HiddenLayer', 'OutputLayer'],
                         activations=[None, T.nnet.sigmoid, None],
                         rng=rng,
                         disable_logging=True,
                         )

    trainer = ModelTrainer(
        model,
        train_data,
        eta=FixedLearnRate(0.1),
        max_epochs=5,
    )

    return model, trainer, xtrain, ytrain


def test_or_gate_trainer_result(model):
    """ Test training with simple OR gate correct results"""
    model, trainer, xtrain, ytrain = model
    model.save = MagicMock()

    trainer.train()

    # Get the outputs
    ycheck = model.predict(xtrain.eval())
    np.testing.assert_almost_equal(
        ycheck, ytrain.eval(), decimal=0)


def test_or_gate_pickle(model):
    """ Test training with simple OR gate file pickling"""
    model, trainer, xtrain, ytrain = model
    model.save = MagicMock()

    trainer.train()

    model.save.assert_called_once()


def test_mini_batches():
    """ Generate the OR gate training model - minibatches """
    xtrain = theano.shared(np.array([
        [1, 0],
        [1, 1],
        [0, 0],
        [1, 0],
        [0, 0],
        [0, 0],
    ], dtype=theano.config.floatX), borrow=True)

    ytrain = theano.shared(np.array([
        [1],
        [1],
        [0],
        [1],
        [0],
        [0],
    ], dtype=theano.config.floatX), borrow=True)

    # Train to zero error - do not use validation set
    train_data = (xtrain, xtrain, ytrain, ytrain)

    # Construct simple model
    x = T.matrix()
    y = T.matrix()

    units = [2, 2, 1]
    rng = np.random.RandomState(0)

    model = ModelBuilder(x, y,
                         units=units,
                         layers=['InputLayer', 'HiddenLayer', 'OutputLayer'],
                         activations=[None, T.tanh, None],
                         rng=rng,
                         disable_logging=True,
                         )
    model.save = MagicMock()

    trainer = ModelTrainer(
        model,
        train_data,
        eta=FixedLearnRate(0.1),
        max_epochs=100,
        batch_size=6
    )

    assert(trainer.batch_size is not None)
    assert(trainer.num_train_batches == 1)

    # Just check training was completed correctly
    trainer.train()

    # Get the outputs
    ycheck = model.predict(xtrain.eval())
    np.testing.assert_almost_equal(
        ycheck, ytrain.eval(), decimal=0)


def test_dropout():
    """ Generate the OR gate training model - minibatches """
    xtrain = theano.shared(np.array([
        [1, 0],
        [1, 1],
        [0, 0],
        [1, 0],
        [0, 0],
        [0, 0],
    ], dtype=theano.config.floatX), borrow=True)

    ytrain = theano.shared(np.array([
        [1],
        [1],
        [0],
        [1],
        [0],
        [0],
    ], dtype=theano.config.floatX), borrow=True)

    xvalid = theano.shared(np.array([
        [2, 0],
        [2, 1],
        [2, 0],
        [2, 0],
        [2, 0],
        [2, 0],
    ], dtype=theano.config.floatX), borrow=True)

    yvalid = theano.shared(np.array([
        [2],
        [2],
        [0],
        [2],
        [0],
        [0],
    ], dtype=theano.config.floatX), borrow=True)

    # Train to zero error - do not use validation set
    train_data = (xtrain, xtrain, yvalid, yvalid)

    # Construct simple model
    x = T.matrix()
    y = T.matrix()

    units = [2, 2, 2, 1]
    rng = np.random.RandomState(0)

    layers = [
        'InputLayer',
        'HiddenLayer',
        'DropoutLayer',
        'OutputLayer',
    ]

    activations = [
        None,
        T.tanh,
        None,
        None,
    ]

    model = ModelBuilder(x, y,
                         units=units,
                         layers=layers,
                         activations=activations,
                         rng=rng,
                         disable_logging=True,
                         )
    model.save = MagicMock()

    with patch('theano.function') as func_patch:
        trainer = ModelTrainer(
            model,
            train_data,
            eta=FixedLearnRate(0.1),
            max_epochs=100,
            batch_size=6
        )

        assert(func_patch.call_count == 2)
        assert(trainer.train_givens.get(model.enable_drop))
        assert(not trainer.valid_givens.get(model.enable_drop))
