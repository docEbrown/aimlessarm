aimlessarm
==========

|License: GPL v3| |Build Status| |codecov|

Templated neural network library using Theano

-  Free software: GNU General Public License v3
-  Documentation: https://aimlessarm.readthedocs.io.

## TODO
-------

-  Refactor
-  Compute Pool layer shape from previous layer
-  Add some data prep functions e.g. normalise
-  Pypi

## Features
-----------

-  Stuff 
-  Documentation

## Credits
----------

This package was created with Cookiecutter\_ and the
``audreyr/cookiecutter-pypackage``\ \_ project template.

.. *Cookiecutter: https://github.com/audreyr/cookiecutter ..
*\ ``audreyr/cookiecutter-pypackage``:
https://github.com/audreyr/cookiecutter-pypackage

.. |License: GPL v3| image:: https://img.shields.io/badge/License-GPL%20v3-blue.svg
   :target: https://www.gnu.org/licenses/gpl-3.0
.. |Build Status| image:: https://travis-ci.org/doc-E-brown/aimlessarm.svg?branch=master
   :target: https://travis-ci.org/doc-E-brown/aimlessarm
.. |codecov| image:: https://codecov.io/gh/doc-E-brown/AimlessArm/branch/master/graph/badge.svg
   :target: https://codecov.io/gh/doc-E-brown/AimlessArm
