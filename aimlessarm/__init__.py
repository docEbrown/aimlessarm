# -*- coding: utf-8 -*-

"""Top-level package for AimlessArm."""

__author__ = """Ben Johnston"""
__email__ = 'bjohnston@neomailbox.net'

from .layers import (
    BaseLayer,
    InputLayer,
    HiddenLayer,
    OutputLayer,
    ConvLayer,
    PoolLayer,
)

from .modelBuilder import (
    ModelBuilder,
)

from .modelTrainer import (
    ModelTrainer,
)

from .resources import (
    mlpMNIST,
    alexMNIST,
)


from ._version import get_versions
__version__ = get_versions()['version']
del get_versions
