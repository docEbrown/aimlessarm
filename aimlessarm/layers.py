#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""
Layers Module



:author: Ben Johnston

"""

# Imports

import theano
import theano.tensor as T
import numpy as np
from theano.tensor.signal import pool
from theano.ifelse import ifelse


def get_name(obj):
    """ Get the name of an activation object """

    if hasattr(obj, '__qualname__'):
        return obj.__qualname__
    elif hasattr(obj, 'name'):
        # Correct tanh
        if obj == T.tanh:
            return 'tanh'
        return obj.name
    elif obj is None:
        return "None"


class BaseLayer(object):
    """Base Layer Class

    The base layer class provides the basic structure of a layer
    in a neural network with all of the required components. Each
    layer will provide the following attributes:

        * layer.input: a reference to the input tensor
        * layer.input_shape: the shape of any inputs to the layer
        * layer.output_shape: the shape of any outputs from the layer 
        * layer.rng: a reference to the random number generator
        * layer.activation: the activation function for the layer
        * layer.W: the network weights for the layer
        * layer.b: the bias weights for the layer
        * layer.output: the computed output for the layer given the inputs,
            weights and activation function
    """

    def __init__(self, **kwargs):
        """Initialise the Base Layer Object

        Parameters
        ----------

        tensor_in: theano.tensor.var.TensorVariable
            The input tensor for the layer

        input_shape: [int]
            Shape of input units to the layer

        output_shape: [int]
            Shape of output units to the layer

        rng: random number generator object
            e.g. numpy.random.Randomstate

        Returns
        ----------
        None

        """

        self.__qualname__ = "BaseLayer"

        # Check for required inputs
        if 'tensor_in' not in kwargs:
            raise ValueError('Missing tensor_in parameter')
        if 'input_shape' not in kwargs:
            raise ValueError('Missing input_shape parameter')

        # Check default inputs
        if isinstance(kwargs['input_shape'], int):
            kwargs['input_shape'] = [kwargs['input_shape']]

        if isinstance(kwargs['output_shape'], int):
            kwargs['output_shape'] = [kwargs['output_shape']]

        if 'activation' not in kwargs:
            kwargs['activation'] = None

        # Save references
        for arg in kwargs:
            self.__setattr__(arg, kwargs[arg])

        """
        # Store for later reference if required
        self.input = tensor_in
        self.input_shape = input_shape
        self.output_shape = output_shape
        self.rng = rng
        self.activation = activation
        """

        # If shapes not provided cannot complete
        if self.input_shape and self.output_shape:
            self._initialise_weights()

            if hasattr(self, 'W') and hasattr(self, 'b'):
                self.output = T.dot(self.tensor_in, self.W) + self.b

                if self.activation:
                    self.output = self.activation(self.output)
            else:
                self.output = self.tensor_in

    def _compute_bounds(self):
        """Compute the bounds for initialising weights

        Parameters
        ----------
        None

        Returns
        ----------
        (n_in, n_out, bounds)

        W_shape: int
            Shape of weights matrix for layer

        b_shape: int
            Number of output units from the layer

        bounds: float
            Limits for weights to be initialised randomly

        """

        # Check for CNN input - 4 element list
        # [# Batches in, # Filters_out, X, Y]
        if len(self.input_shape) == 4:
            n_in = np.prod(self.input_shape[1:])
        else:
            n_in = self.input_shape[0]

        n_out = self.output_shape[0]

        bounds = np.sqrt(6. / (n_in + n_out))

        W_shape = (n_in, n_out)
        b_shape = (n_out,)

        # TODO if sigmoid activation is used bounds *= 4

        return W_shape, b_shape, bounds

    def _initialise_weights(self):
        """Initialise the weights of the layer

        Default to simple nnet hidden layer

        Parameters
        ----------
        None

        Returns
        ----------
        None

        """

        W_shape, b_shape, bounds = self._compute_bounds()

        if not hasattr(self, 'rng'):
            raise ValueError('Missing rng input parameter')

        # TODO modify to allow use of theano.tensor.shared_randomstreams.RandomStreams
        # self.W = self.rng.uniform(low=-bounds, high=bounds, size=W_shape)
        # self.b = self.rng.uniform(low=-bounds, high=bounds, size=b_shape)
        self.W = theano.shared(
            np.asarray(
                self.rng.uniform(low=-bounds, high=bounds, size=W_shape),
                dtype=theano.config.floatX),
        )

        self.b = theano.shared(
            np.asarray(
                self.rng.uniform(low=-bounds, high=bounds, size=b_shape),
                dtype=theano.config.floatX))

        self.params = [self.W, self.b]

    def __str__(self):
        """Print out the structure of the layer"""

        msg = "{:<10}: {}x{} {:<10}{}".format(
            self.__qualname__,
            self.input_shape,
            self.output_shape,
            "Activation: ",
            get_name(self.activation),
        )

        return msg


class InputLayer(BaseLayer):
    """ Input Layer Class """

    def __init__(self, **kwargs):

        super().__init__(**kwargs)
        self.__qualname__ = "InputLayer"
        self.output = self.tensor_in

    def _initialise_weights(self):
        """ Do not initialise any weights """
        pass

    def _compute_bounds(self):
        """ Do not compute any bounds """

        return None, None, None

    def __str__(self):
        """Print out the structure of the layer"""

        msg = "{:<10}: {} {:<10}{}".format(
            self.__qualname__,
            self.input_shape,
            "Activation: ",
            get_name(self.activation),
        )

        return msg


class HiddenLayer(BaseLayer):
    """Hidden Layer Class"""

    def __init__(self, **kwargs):
        """Initialise the Hidden Layer Object

        Parameters
        ----------

        tensor_in: theano.tensor.var.TensorVariable
            The input tensor for the layer

        n_in: int
            Number of input units to the layer 

        n_out: [int]
            Number of output units to the layer 

        rng: random number generator object
            e.g. numpy.random.Randomstate

        Returns
        ----------
        None

        """

        # Check default inputs
        # TODO can do this check better
        if isinstance(kwargs['input_shape'], int):
            kwargs['input_shape'] = [kwargs['input_shape']]

        if isinstance(kwargs['output_shape'], int):
            kwargs['output_shape'] = [kwargs['output_shape']]

        # If the input layer is a Conv, flatten
        if (len(kwargs['input_shape']) == 4):
            kwargs['tensor_in'] = kwargs['tensor_in'].flatten((2))

        super().__init__(**kwargs)
        self.__qualname__ = "HiddenLayer"


class OutputLayer(HiddenLayer):
    """ An alias of HiddenLayer """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__qualname__ = "OutputLayer"


class ConvLayer(BaseLayer):

    def __init__(self, **kwargs):

        # Check for border_mode and filter_shape
        if 'filter_shape' not in kwargs:
            raise ValueError('ConvLayer missing filter_shape argument')

        if 'border_mode' not in kwargs:
            kwargs['border_mode'] = 'valid'

        if 'activation' not in kwargs:
            kwargs['activation'] = T.nnet.relu

        super().__init__(**kwargs)
        self.__qualname__ = "ConvLayer"

        # Weights initialised in super

        conv = T.nnet.conv2d(
            input=self.tensor_in,
            filters=self.W,
            input_shape=self.input_shape,
            filter_shape=self.filter_shape,
            border_mode=self.border_mode,
        )

        self.output = conv + self.b.dimshuffle('x', 0, 'x', 'x')

        # Replace the output
        if self.activation:
            self.output = self.activation(self.output)

    def _compute_bounds(self):
        """Compute the bounds for initialising weights

        Parameters
        ----------
        None

        Returns
        ----------
        (n_in, n_out, bounds)

        n_in: int
            Number of input units to the layer

        n_out: int
            Number of output units from the layer

        bounds: float
            Limits for weights to be initialised randomly

        """

        n_in = np.prod(self.input_shape[1:])
        n_out = self.filter_shape[0] *\
            (self.input_shape[2] - self.filter_shape[2] + 1) *\
            (self.input_shape[3] - self.filter_shape[3] + 1)

        bounds = np.sqrt(6. / (n_in + n_out))

        return self.filter_shape, self.filter_shape[0], bounds

    def __str__(self):
        """Print out the structure of the layer"""

        msg = "{:<10}: {}o{}x{} {:<10}{}".format(
            self.__qualname__,
            self.input_shape,
            self.filter_shape,
            self.output_shape,
            "Activation: ",
            get_name(self.activation),
        )

        return msg


class PoolLayer(BaseLayer):

    def __init__(self, **kwargs):
        # TODO build function for this
        # Default inputs
        if 'ws' not in kwargs:
            kwargs['ws'] = (2, 2)

        if 'ignore_border' not in kwargs:
            kwargs['ignore_border'] = False

        if 'stride' not in kwargs:
            kwargs['stride'] = None

        if 'mode' not in kwargs:
            kwargs['mode'] = 'max'

        if 'pad' not in kwargs:
            kwargs['pad'] = (0, 0)

        super().__init__(**kwargs)

        self.__qualname__ = "PoolLayer"
        self.output = pool.pool_2d(
            input=self.tensor_in,
            ws=self.ws,
            ignore_border=self.ignore_border,
            stride=self.stride,
            pad=self.pad,
            mode=self.mode,
        )

        if self.activation is not None:
            self.output = self.activation(self.output)

    def _initialise_weights(self):  # pragma: no cover
        """ Do not initialise any weights """
        pass

    def _compute_bounds(self):  # pragma: no cover
        """ Do not compute any bounds """

        return None, None, None


class DropoutLayer(BaseLayer):
    """ Dropout Layer

    Designed as per the original paper by Hinton et al in 2012
    Improving neural networks by preventing co-adaptation of feature detectors
    http://arxiv.org/abs/1207.0580

    For each training sample, each input or hidden unit in the network is randomly
    omitted with a probability of p %.  For validation or test samples, the output
    of the dropout layer is reduced by (1 - p)% to compensate for the fact that there
    are more active units than were present during training.  Hinton et al describe
    dividing the weights of the previous layer by a factor of 2 for 50% dropout,
    but this amounts to the same as dividing the output of the dropout layer
    by the same amount.
    """

    def __init__(self, **kwargs):

        # Bypass need for input shape
        # kwargs['input_shape'] = kwargs['tensor_in'].shape
        kwargs['output_shape'] = kwargs['input_shape']

        super().__init__(**kwargs)

        self.__qualname__ = "DropoutLayer"

        # Check for required variables
        if 'p' not in kwargs:
            kwargs['p'] = 0.5
        if 'enable' not in kwargs:
            raise AttributeError("DropoutLayer missing enable tensor")

        # Assign class variables
        self.p = kwargs['p']
        self.enable = kwargs['enable']
        self.mask = self.rng.binomial(
            n=1, p=(1 - self.p), size=self.input_shape)

        self.output = ifelse(self.enable,
                             self.tensor_in *
                             T.cast(self.mask, theano.config.floatX),
                             (1 - self.p) * self.tensor_in)

    def _initialise_weights(self):  # pragma: no cover
        """ Do not initialise any weights """
        pass

    def _compute_bounds(self):  # pragma: no cover
        """ Do not compute any bounds """

        return None, None, None

    def __str__(self):

        return "Dropout Layer: p = {:.2f}".format(self.p)
