# -*- coding: utf-8 -*-

"""Console script for aimlessarm."""

import click


@click.command()
def main(args=None):
    """Console script for aimlessarm."""
    click.echo("Replace this message by putting your code into "
               "aimlessarm.cli.main")
    click.echo("See click documentation at http://click.pocoo.org/")


if __name__ == "__main__":
    main()
